<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view ('Profile.profile');

        $data = Profile::all();
        return view('Profile/profile', [
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Profile::find($id);
        $action = 'show';
        return view('Profile/profile', compact('detail', 'action'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Profile::find($id);
        $action = 'edit';
        return view('Profile/editProfile', compact('detail', 'action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $updateprofile = Profile::find($request->id);

        $this->validate($request, [
            'name' => 'required|max:24|regex:/^[^\W\d_]+$/',
            'address' => 'required|max:50',
        ], [
            'name.regex' => 'cannot contain numbers or symbols',
        ]);

        if ($updateprofile->email == $request->email) {
            $this->validate($request, [
                'email' => 'required|max:100|email',
            ],);
        } else {
            $this->validate($request, [
                'email' => 'required|max:100|email|unique:profiles',
            ]);
        }

        if ($updateprofile->username == $request->username) {
            $this->validate($request, [
                'username' => 'required|max:25|regex:/^\S+$/',
            ],);
        } else {
            $this->validate($request, [
                'username' => 'required|max:25|unique:profiles|regex:/^\S+$/',
            ], [
                'username.regex' => 'cannot contain spacing',
            ]);
        }

        $updateprofile->id = $request->id;
        $updateprofile->name = $request->name;
        $updateprofile->email = $request->email;
        $updateprofile->username = $request->username;
        $updateprofile->address = $request->address;
        $updateprofile->password = $request->password;
        $updateprofile->photo = $request->photo;

        if ($updateprofile->save()) {
            // return redirect('/profile/' . $request->id);
            return redirect('/')->with('success', 'data berhasil diubah');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

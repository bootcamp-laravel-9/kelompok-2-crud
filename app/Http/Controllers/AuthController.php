<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function show(Request $request)
    {
        $this->validate($request, [
            'password' => 'regex:/^.*(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$/'
        ], [
            'password.regex' => 'must contain one lowercase, uppercase, and symbol'
        ]);

        $profile = Profile::where('email', $request->email)->first();

        if (!$profile) {
            // return redirect()->with('error', 'Email atau password salah')->route('login');
            return back()->with('error', 'Email atau password salah');
        }

        $isValidPassword = Hash::check($request->password, $profile->password);

        if (!$isValidPassword) {
            // return redirect()->with('error', 'Email atau password salah')->route('login');
            return back()->with('error', 'Email atau password salah');
        }

        $token = $profile->createToken(config('app.name'))->plainTextToken;
        $request->session()->put('sessionLogin', $token);
        $request->session()->put('profileId', $profile->id);
        return redirect('/profile/' . $profile->id);
    }

    public function register()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:24|regex:/^[^\W\d_]+$/',
            'username' => 'required|max:25|unique:profiles|regex:/^\S+$/',
            'address' => 'required|max:50',
            'photo' => 'required|url',
            'email' => 'required|max:100|email|unique:profiles',
            'password' => 'required|min:8|max:15|regex:/^.*(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$/',
            'password_confirmation' => 'required|required_with:password|same:password'
        ], [
            'username.regex' => 'cannot contain spacing',
            'name.regex' => 'cannot contain numbers or symbols',
            'password.regex' => 'must contain one lowercase, uppercase, and symbol'
        ]);

        $profile = Profile::create([
            'name' => $request->name,
            'username' => $request->username,
            'address' => $request->address,
            'photo' => $request->photo,
            'email' => $request->email,
            'password' => Hash::make($request['password'])
        ]);

        return redirect('login');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}

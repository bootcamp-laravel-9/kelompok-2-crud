<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['isNotAuth']], function () use ($router) {
    //routing ke halaman view "welcome"
    // Route::get('/', function () {
    //     return view('welcome');
    // });

    // Route::get('/', function () {
    //     return view('dashboard.index');
    // });
    Route::get('/', [DashboardController::class, 'index']);
    
    //logout routing
    Route::get('/logout', [AuthController::class, 'logout']);

    Route::get('/profile/{id}', [ProfileController::class, 'show']);
    Route::get('/editProfile/{id}', [ProfileController::class, 'edit']);
    Route::post('/edit-profile-proses', [ProfileController::class, 'update']);
});

//cek jika ada login session akan redirect ke main page
Route::group(['middleware' => ['isAuth']], function () use ($router) {
    Route::get('/login', [AuthController::class, 'login']);
    Route::post('/login', [AuthController::class, 'show']);
    Route::get('/register', [AuthController::class, 'register']);
    Route::post('/register', [AuthController::class, 'store']);
});

//Route::put('/edit-profile-proses', 'UserController@update');
//Route::get('/member-detail/{id}', 'UserController@show');
//Route::get('/profile', 'ProfileController@index');
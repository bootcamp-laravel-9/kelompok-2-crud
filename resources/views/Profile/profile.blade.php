@extends('layout.main')
{{-- section ('('nama yield', 'valuenya')') --}}
@section('menu-title', 'Detail Member')
@section('menu-profile', 'active')
@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        
        <form method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <input name="profileId" type="hidden" class="form-control" id="exampleInputEmail1" value="{{ $detail->id }}" {{ $action == 'show'?'disabled':'' }}>
                </div>
                <div class="text-center">
                    <img src="{{ $detail->photo }}" class="img-circle elevation-2" style="width: 200px; height: 200px;" alt="User Image">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nickname</label>
                    <input name="profileName" type="text" class="form-control" id="exampleInputEmail1" value="{{ $detail->name }}" {{ $action == 'show'?'disabled':'' }}>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input name="profileEmail" type="email" class="form-control" id="exampleInputEmail1" value="{{ $detail->email }}"  {{ $action == 'show'?'disabled':'' }}>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input name="profileUsername" type="text" class="form-control" id="exampleInputEmail1" value="{{ $detail->username }}"  {{ $action == 'show'?'disabled':'' }}>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <input name="profileAddress" type="text" class="form-control" id="exampleInputEmail1" value="{{ $detail->address }}"  {{ $action == 'show'?'disabled':'' }}>
                </div>

                <div class="card">
                    <a href="{{ url('/editProfile/'.$detail->id) }}" class="btn btn-primary">Edit</a>
                </div>

                {{-- @if($action!='show')
                    <div class="card">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                @endif --}}
            </div>
        </form>
    </div>
</body>
</html>
@endsection
@extends('layout.main')
{{-- section ('('nama yield', 'valuenya')') --}}
@section('menu-title', 'Detail Member')
@section('menu-bootcamp', 'active')
@section('content')

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>

    <body>
        <div>

            <form action="{{ url('/edit-profile-proses/') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <input name="id" type="hidden" class="form-control" id="exampleInputEmail1"
                            value="{{ $detail->id }}">
                    </div>
                    <div class="text-center">
                        <img src="{{ $detail->photo }}" class="img-circle elevation-2" style="width: 200px; height: 200px;"
                            alt="User Image">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nickname</label>
                        <input name="name" type="text" class="form-control" id="name"
                            value="{{ $detail->name }}">
                        @error('name')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input name="email" type="text" class="form-control" id="email"
                            value="{{ $detail->email }}">
                        @error('email')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input name="username" type="text" class="form-control" id="username"
                            value="{{ $detail->username }}">
                        @error('username')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input name="address" type="text" class="form-control" id="address"
                            value="{{ $detail->address }}">
                        @error('address')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password</label>
                        <input name="password" readonly type="text" class="form-control" id="exampleInputEmail1"
                            value="{{ $detail->password }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Photo</label>
                        <input name="photo" readonly type="text" class="form-control" id="exampleInputEmail1"
                            value="{{ $detail->photo }}">
                    </div>

                    <div class="card">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                    {{-- @if ($action != 'show')
                    <div class="card">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                @endif --}}
                </div>
            </form>
        </div>
    </body>

    </html>
@endsection

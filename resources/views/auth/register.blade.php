@extends('layout.authLayout')

@section('content')
    <div class="register-box">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="../../index2.html" class="h1"><b>Social Y</b></a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Register a new Account</p>

                <form action={{ url('/register') }} method="post">
                    @csrf
                    <div class="mb-3">
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Email" name="email" id="email"
                                value="{{ old('email') }}" required>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        @error('email')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="mb-3">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Username" name="username" id="username"
                                value="{{ old('username') }}" required>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-users-cog"></span>
                                </div>
                            </div>
                        </div>
                        @error('username')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="mb-3">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Address" name="address" id="address"
                                value="{{ old('address') }}" required>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-map-marker-alt"></span>
                                </div>
                            </div>
                        </div>
                        @error('address')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="mb-3">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Full name" name="name" id="name"
                                value="{{ old('name') }}" required>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                        </div>
                        @error('name')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="mb-3">
                        <div class="input-group">
                            <input type="password" class="form-control" placeholder="Password" name="password"
                                id="password" required>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        @error('password')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="mb-3">
                        <div class="input-group">
                            <input type="password" class="form-control" placeholder="Confirm Password"
                                name="password_confirmation" id="password_confirmation" required>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        @error('password_confirmation')
                            {{ $message }}
                        @enderror
                    </div>
                    <div class="mb-3">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="photo url" name="photo" id="photo"
                                value="{{ old('photo') }}" required>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-image"></span>
                                </div>
                            </div>
                        </div>
                        @error('photo')
                            {{ $message }}
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Register</button>
                </form>

                <a href={{ url('/login') }} class="text-center">I already have a account</a>
            </div>
            <!-- /.form-box -->
        </div><!-- /.card -->
    </div>
    <!-- /.register-box -->
@endsection
